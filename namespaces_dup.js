var namespaces_dup =
[
    [ "BNO055", null, [
      [ "BNO055", "classBNO055_1_1BNO055.html", "classBNO055_1_1BNO055" ]
    ] ],
    [ "closed_loop_controller", null, [
      [ "ClosedLoop", "classclosed__loop__controller_1_1ClosedLoop.html", null ]
    ] ],
    [ "DRV8847", null, [
      [ "DRV8847", "classDRV8847_1_1DRV8847.html", "classDRV8847_1_1DRV8847" ],
      [ "Motor", "classDRV8847_1_1Motor.html", "classDRV8847_1_1Motor" ]
    ] ],
    [ "encoder", null, [
      [ "Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "HW_0x01", "namespaceHW__0x01.html", [
      [ "getChange", "namespaceHW__0x01.html#abfa185fb49bdf353e198ad02ab389654", null ],
      [ "cng", "namespaceHW__0x01.html#a868cbf54029436fb6e6d4b6aa8146051", null ],
      [ "cngVal", "namespaceHW__0x01.html#a4740893c3a074a5bd64df64e0d401f45", null ],
      [ "denoms", "namespaceHW__0x01.html#a133ac2b808698b3316db8c9211667639", null ],
      [ "pmt", "namespaceHW__0x01.html#a4dbb0a8dbed6fc8408ff541a2b051e8e", null ],
      [ "pmtVal", "namespaceHW__0x01.html#ad638f997e0a837494e540d388d0276ab", null ],
      [ "price", "namespaceHW__0x01.html#a6ffa02ed681f39b50cd02eb097c3f254", null ],
      [ "test_cases", "namespaceHW__0x01.html#a73f90f8633cce5f280ebeb6c31b8abf8", null ]
    ] ],
    [ "IMU_driver", null, [
      [ "BNO055", "classIMU__driver_1_1BNO055.html", null ]
    ] ],
    [ "main", null, [
      [ "moto", "HW01_2main_8py.html#a385bbf44e78f0a990eafaf8d62b3b606", null ]
    ] ],
    [ "motor", null, [
      [ "Motor", "classmotor_1_1Motor.html", "classmotor_1_1Motor" ]
    ] ],
    [ "panel", null, [
      [ "Panel", "classpanel_1_1Panel.html", null ]
    ] ],
    [ "shares", null, [
      [ "Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ],
      [ "Share", "classshares_1_1Share.html", "classshares_1_1Share" ]
    ] ],
    [ "task_closed_loop_controller", null, [
      [ "Task_Closed_Loop_Controller", "classtask__closed__loop__controller_1_1Task__Closed__Loop__Controller.html", "classtask__closed__loop__controller_1_1Task__Closed__Loop__Controller" ]
    ] ],
    [ "task_encoder", null, [
      [ "Task_Encoder", "classtask__encoder_1_1Task__Encoder.html", "classtask__encoder_1_1Task__Encoder" ],
      [ "S0_init", "Lab_012_2task__encoder_8py.html#a118da909b013744f2b2ef4febcca4e32", null ]
    ] ],
    [ "task_IMU", null, [
      [ "Task_IMU", "classtask__IMU_1_1Task__IMU.html", "classtask__IMU_1_1Task__IMU" ]
    ] ],
    [ "task_motor", null, [
      [ "Task_Motor", "classtask__motor_1_1Task__Motor.html", "classtask__motor_1_1Task__Motor" ],
      [ "S0_init", "Lab_013_2task__motor_8py.html#aaa32437a8951515fe5ce41df8fe1e298", null ]
    ] ],
    [ "task_motorDriver", null, [
      [ "Task_motorDriver", "classtask__motorDriver_1_1Task__motorDriver.html", "classtask__motorDriver_1_1Task__motorDriver" ]
    ] ],
    [ "task_panel", null, [
      [ "Task_Panel", "classtask__panel_1_1Task__Panel.html", null ]
    ] ],
    [ "task_user", null, [
      [ "Task_User", "classtask__user_1_1Task__User.html", "classtask__user_1_1Task__User" ]
    ] ]
];