var searchData=
[
  ['gatherdata_0',['gatherData',['../classtask__user_1_1Task__User.html#afe2ae3f36f37b98bb2d72750055ec48f',1,'task_user::Task_User']]],
  ['get_1',['get',['../classshares_1_1Queue.html#a45835daf8ee60391cca9667a942ade25',1,'shares.Queue.get(self)'],['../classshares_1_1Queue.html#a45835daf8ee60391cca9667a942ade25',1,'shares.Queue.get(self)'],['../classshares_1_1Queue.html#a45835daf8ee60391cca9667a942ade25',1,'shares.Queue.get(self)'],['../classshares_1_1Queue.html#a45835daf8ee60391cca9667a942ade25',1,'shares.Queue.get(self)']]],
  ['get_5fcalibration_5fcoefficient_2',['get_calibration_coefficient',['../classBNO055_1_1BNO055.html#a37d99d3467b67b4600b4b70b31ab1dec',1,'BNO055::BNO055']]],
  ['get_5fcalibration_5fdata_3',['get_calibration_data',['../classBNO055_1_1BNO055.html#a9885a9af876bfd6aaace6f012d7c30aa',1,'BNO055::BNO055']]],
  ['get_5fdelta_4',['get_delta',['../classencoder_1_1Encoder.html#a2f451b6cb3e85e03d45e0ac097e29a29',1,'encoder.Encoder.get_delta(self)'],['../classencoder_1_1Encoder.html#a2f451b6cb3e85e03d45e0ac097e29a29',1,'encoder.Encoder.get_delta(self)'],['../classencoder_1_1Encoder.html#a2f451b6cb3e85e03d45e0ac097e29a29',1,'encoder.Encoder.get_delta(self)']]],
  ['get_5fencoder_5fid_5',['get_encoder_ID',['../classencoder_1_1Encoder.html#a9adcb242bfa77d5d5901f56a1053ab6d',1,'encoder::Encoder']]],
  ['get_5fposition_6',['get_position',['../classencoder_1_1Encoder.html#abc44b0bb3d2ee93571f00d6bab5e7c53',1,'encoder.Encoder.get_position(self)'],['../classencoder_1_1Encoder.html#abc44b0bb3d2ee93571f00d6bab5e7c53',1,'encoder.Encoder.get_position(self)'],['../classencoder_1_1Encoder.html#abc44b0bb3d2ee93571f00d6bab5e7c53',1,'encoder.Encoder.get_position(self)'],['../classencoder_1_1Encoder.html#abc44b0bb3d2ee93571f00d6bab5e7c53',1,'encoder.Encoder.get_position(self)']]],
  ['getchange_7',['getChange',['../namespaceHW__0x01.html#abfa185fb49bdf353e198ad02ab389654',1,'HW_0x01']]]
];
